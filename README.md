# Avris Boilerplate

Boilerplate code for a PHP library

## Usage

   composer create-project avris/boilerplate myproject

## Tests

    vendor/bin/phpunit

## Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
