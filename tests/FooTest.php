<?php

namespace App;

use PHPUnit\Framework\TestCase;

class FooTest extends TestCase
{
    /** @var Foo */
    private $foo;

    public function setUp()
    {
        $this->foo = new Foo();
    }

    /**
     * @dataProvider barProvider
     */
    public function testBar(int $input, int $expectedOutput)
    {
        $this->assertEquals($expectedOutput, $this->foo->bar($input));
    }

    public function barProvider()
    {
        yield [7, 8];
        yield [8, 9];
    }
}
